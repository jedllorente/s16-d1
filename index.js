// -While loop-
// let count = 5; // number of iteration, number of times how many we repeat our code.

/*while (condition) { // condition - e valuates  a given code if it's true or false. if condition is true
    // loop will start and o=continue our iteration or the repetition of our block. but if condition is false loop will stop.
    // block of code
    // counter for out iteration. this is the reason of consitionous loop/iteration.
};

while (count != 0) {
    console.log('Sylvan');
    count--;

}

let number = 1;
while (number <= 5) {
    console.log(number);
    number++;
}*/

/* Instruction: With a given array, kindly print each element using while loop */

/*let fruits = ['Banana', 'Mango'];
let num = 0;
while (num <= 1) {
    console.log(fruits[num]);
    num++;
}


let mobilePhones = ['samsung', 'iPhone', 'Xiaomi', 'Realme', 'Huawei', 'Google', 'Asus', 'Nokia', 'Cherry Mobile'];

let indexNumber = 0;

while (indexNumber <= mobilePhones.length - 1) {
    console.log(mobilePhones[indexNumber]);
    indexNumber++;
}


let countB = 6;

do {
    console.log(`Do-while count ${countB}`);
    countB--;
} while (countB == 7);


// Versus

while (countB == 7) {
    console.log(`Do-While count ${countB}`);
    countB--;
}

let computerBrands = ['Apple', 'HP', 'Asus', 'Lenovo', 'Acer', 'Dell', 'Huawei'];
do {
    console.log(computerBrands[indexNumber]);
    indexNumber++;
} while (indexNumber <= computerBrands.length - 1);

for (let indexNumber = 0; indexNumber < computerBrands.length - 1; indexNumber++) {
    console.log(computerBrands[indexNumber]);
};

let colors = ['Red', 'Green', 'Blue', 'Yellow', 'Purple', 'White', 'Black'];

for (let i = 0; i <= colors.length - 1; i++) {
    console.log(colors[i]);
}


let ages = [18, 19, 20, 21, 22, 23, 24, 25];

for (let i = 0; i <= ages.length - 1; i++) {
    if (ages[i] == 21 || ages[i] == 18) {
        continue;
    } else {
        console.log(ages[i]);
    }
}*/

/* Continue & Break */

/*let studentName = ['Den', 'Jayson', 'Marvin', 'Rommel'];
for (let i = 0; i < studentName.length; i++) {
    if (studentName[i] == 'Jayson') {
        console.log(studentName[i]);
        break;
    }
}*/

//Coding Challenge - Scope (Loops, Continue and Break)
// You can add the solution under our s16/d1/index.js only, no need to create a separate folder for this. Kindly push your solutions once you are done
//Then link your activity gitlab repo under Boodle WD078-16

/*
Instructions:

1. Given the array 'adultAge', display only adult age range on the console.

-- the goal of this activity is to exclude values that are not in the range of adult age. Using the tool loops, continue or break, 
the students must display only the given sample output below on their console.
*/
let adultAge = [20, 23, 33, 27, 18, 19, 70, 15, 55, 63, 85, 12, 19];


for (var i = 0; i < adultAge.length; i++) {
    if (adultAge[i] < 20) {
        continue;
    } else {
        console.log(adultAge[i]);
    }
}
/*
    Sample output:
  
  20
  23
  33
  27
  70
  55
  63
  85
*/

/*
Instructions:

2. Given an array 'students' and a function searchStudent, create a solution that, once a function is invoked with a given name of student as its argument,
    the function will start to loop and search for the student on a given array and once there's found it will print it on the console 
  and stop the execution of loop. 
  
  -- the goal of this activity is to print only the value needed based on the argument given. Use loop, and continue or break
*/
let students = ['Gary', 'Amelie', 'Anne', 'Jazz', 'Preina', 'James', 'Kelly', 'Diane', 'Lucy', 'Vanessa', 'Kim', 'Francine'];

function searchStudent(studentName){
    //add solutions here
  //you can refer on our last discussion yesterday
  for (let i = 0; i < students.length; i++) {
    if (students[i] == studentName) {
        console.log(students[i]);
        break;
    } else {}
  }
}
searchStudent('Jazz'); //invoked function with a given argument 'Jazz'

/*
    Sample output:
  
  Jazz
*/